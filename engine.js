const express = require('express'),
      app     = express(),
      server  = require('http').createServer(app).listen(3000),
      io      = require('socket.io')(server),
      path    = require('path');
 
// Dossier static
app.use(express.static(__dirname +'/public'));

// Routes
app.get('/', function(req, res) {
   res.sendFile(path.join(__dirname + '/public/views/index.html'));
})
app.get('/getTest', function(req, res) {
	var toEncode=JSON.stringify('YEAH')
   res.send(toEncode);
})
app.post('/postTest', function(req, res) {
	var toEncode=JSON.stringify('YEAH')
   res.send(toEncode);
})

 
// Socket.io
io.on('connection', function(socket){
     
    socket.on('message', function(data){
        //... etc
    });
});